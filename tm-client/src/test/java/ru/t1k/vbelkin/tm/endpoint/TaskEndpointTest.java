package ru.t1k.vbelkin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1k.vbelkin.tm.api.endpoint.IAuthEndpoint;
import ru.t1k.vbelkin.tm.api.endpoint.IProjectEndpoint;
import ru.t1k.vbelkin.tm.api.endpoint.ITaskEndpoint;
import ru.t1k.vbelkin.tm.api.service.IPropertyService;
import ru.t1k.vbelkin.tm.dto.request.*;
import ru.t1k.vbelkin.tm.dto.response.TaskCreateResponse;
import ru.t1k.vbelkin.tm.dto.response.TaskGetByIdResponse;
import ru.t1k.vbelkin.tm.marker.IntegrationCategory;
import ru.t1k.vbelkin.tm.model.Task;
import ru.t1k.vbelkin.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public class TaskEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);
    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);
    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);
    @Nullable
    private Task task;
    private String userToken;

    public Task addTask(final String name, final String description) {
        @Nullable final TaskCreateRequest requestCreateTask = new TaskCreateRequest(userToken);
        requestCreateTask.setName(name);
        requestCreateTask.setDescription(description);
        @NotNull final TaskCreateResponse responseCreateTask = taskEndpoint.createTask(requestCreateTask);
        return responseCreateTask.getTask();
    }

    @Before
    public void setUp() {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin("test");
        request.setPassword("test");
        userToken = authEndpoint.login(request).getToken();
        task = addTask("name", "description");
    }

    @After
    public void tearDown() {
        @Nullable final TaskClearRequest requestClearTask = new TaskClearRequest(userToken);
        taskEndpoint.clearTask(requestClearTask);
        @Nullable final ProjectClearRequest requestClearProject = new ProjectClearRequest(userToken);
        projectEndpoint.clearProject(requestClearProject);
        @NotNull final UserLogoutRequest requestLogout = new UserLogoutRequest(userToken);
        authEndpoint.logout(requestLogout);
    }

    @Test
    public void taskCreate() {
        @Nullable final TaskCreateRequest request = new TaskCreateRequest(userToken);
        request.setName("name");
        request.setDescription("description");
        @Nullable final TaskCreateResponse response = taskEndpoint.createTask(request);
        Task task = response.getTask();
        Assert.assertEquals("name", task.getName());
        Assert.assertEquals("description", task.getDescription());
    }

    @Test
    public void taskClear() {
        Assert.assertFalse(taskEndpoint.listTask(new TaskListRequest(userToken)).getTasks().isEmpty());
        @Nullable final TaskClearRequest request = new TaskClearRequest(userToken);
        taskEndpoint.clearTask(request);
        Assert.assertNull(taskEndpoint.listTask(new TaskListRequest(userToken)).getTasks());
    }

    @Test
    public void taskList() {
        @Nullable final TaskCreateRequest requestTaskCreate = new TaskCreateRequest(userToken);
        requestTaskCreate.setName("name2");
        requestTaskCreate.setDescription("description2");
        taskEndpoint.createTask(requestTaskCreate);
        Assert.assertFalse(taskEndpoint.listTask(new TaskListRequest(userToken)).getTasks().isEmpty());
    }

    @Test
    public void taskShowById() {
        @Nullable final TaskGetByIdRequest requestGetById = new TaskGetByIdRequest(userToken);
        requestGetById.setTaskId(task.getId());
        final @NotNull TaskGetByIdResponse responseGetById = taskEndpoint.getTaskById(requestGetById);
        Assert.assertEquals(task.getId(), responseGetById.getTask().getId());
    }

}
