package ru.t1k.vbelkin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1k.vbelkin.tm.api.endpoint.IAuthEndpoint;
import ru.t1k.vbelkin.tm.api.endpoint.IProjectEndpoint;
import ru.t1k.vbelkin.tm.api.service.IPropertyService;
import ru.t1k.vbelkin.tm.dto.request.*;
import ru.t1k.vbelkin.tm.dto.response.ProjectCreateResponse;
import ru.t1k.vbelkin.tm.dto.response.ProjectGetByIdResponse;
import ru.t1k.vbelkin.tm.enumerated.Status;
import ru.t1k.vbelkin.tm.marker.IntegrationCategory;
import ru.t1k.vbelkin.tm.model.Project;
import ru.t1k.vbelkin.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public final class ProjectEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);
    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);
    @Nullable
    private ProjectCreateResponse responseCreateProject;
    @Nullable
    private String userToken;

    @Before
    public void setUp() {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin("test");
        request.setPassword("test");
        userToken = authEndpoint.login(request).getToken();

        @Nullable final ProjectCreateRequest requestCreateProject = new ProjectCreateRequest(userToken);
        requestCreateProject.setName("name");
        requestCreateProject.setDescription("description");
        responseCreateProject = projectEndpoint.createProject(requestCreateProject);
    }

    @After
    public void tearDown() {
        @Nullable final ProjectClearRequest requestClear = new ProjectClearRequest(userToken);
        projectEndpoint.clearProject(requestClear);
        @NotNull final UserLogoutRequest requestLogout = new UserLogoutRequest(userToken);
        authEndpoint.logout(requestLogout);
    }

    @Test
    public void projectCreate() {
        @Nullable final ProjectCreateRequest request = new ProjectCreateRequest(userToken);
        request.setName("name");
        request.setDescription("description");
        @Nullable final ProjectCreateResponse response = projectEndpoint.createProject(request);
        Project project = response.getProject();
        Assert.assertEquals("name", project.getName());
        Assert.assertEquals("description", project.getDescription());
    }

    @Test
    public void projectChangeStatusById() {
        String projectId = responseCreateProject.getProject().getId();
        @Nullable final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(userToken);
        request.setProjectId(projectId);
        request.setStatus(Status.IN_PROGRESS);
        Project project = projectEndpoint.changeProjectStatusById(request).getProject();
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    public void projectList() {
        @Nullable final ProjectCreateRequest requestProjectCreate = new ProjectCreateRequest(userToken);
        requestProjectCreate.setName("name2");
        requestProjectCreate.setDescription("description2");
        projectEndpoint.createProject(requestProjectCreate);
        Assert.assertFalse(projectEndpoint.listProject(new ProjectListRequest(userToken)).getProjects().isEmpty());
    }

    @Test
    public void projectRemoveById() {
        Assert.assertFalse(projectEndpoint.listProject(new ProjectListRequest(userToken)).getProjects().isEmpty());
        @Nullable final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(userToken);
        request.setProjectId(responseCreateProject.getProject().getId());
        projectEndpoint.removeById(request);
        Assert.assertNotNull(projectEndpoint.listProject(new ProjectListRequest(userToken)).getProjects());
    }

    @Test
    public void projectShowById() {
        @Nullable final ProjectGetByIdRequest requestGetById = new ProjectGetByIdRequest(userToken);
        requestGetById.setProjectId(responseCreateProject.getProject().getId());
        @Nullable final ProjectGetByIdResponse responseGetById = projectEndpoint.getProjectById(requestGetById);
        Assert.assertEquals(responseCreateProject.getProject().getId(), responseGetById.getProject().getId());
    }

    @Test
    public void projectUpdateById() {
        String projectId = responseCreateProject.getProject().getId();
        @Nullable final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(userToken);
        request.setProjectId(projectId);
        request.setDescription("new description");
        request.setName("new name");
        Project project = projectEndpoint.updateById(request).getProject();
        Assert.assertEquals("new name", project.getName());
        Assert.assertEquals("new description", project.getDescription());
    }

}
