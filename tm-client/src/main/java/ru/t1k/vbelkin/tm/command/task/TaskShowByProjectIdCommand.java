package ru.t1k.vbelkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.dto.request.TaskListByProjectIdRequest;
import ru.t1k.vbelkin.tm.model.Task;
import ru.t1k.vbelkin.tm.util.TerminalUtil;

import java.util.List;

public class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-project-id";

    @NotNull
    public static final String DESCRIPTION = "Show task list by project id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(getToken());
        @Nullable final List<Task> tasks = getTaskEndpointClient().listTaskByProjectId(request).getTasks();
        renderTasks(tasks);
    }

}
