package ru.t1k.vbelkin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.dto.request.DataJsonSaveJaxBRequest;

public final class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-save-json-jaxb";

    @NotNull
    private static final String DESCRIPTION = "Save data in json file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE JSON]");
        @NotNull DataJsonSaveJaxBRequest request = new DataJsonSaveJaxBRequest(getToken());
        getDomainEndpointClient().saveDataJsonJaxB(request);
    }

}
