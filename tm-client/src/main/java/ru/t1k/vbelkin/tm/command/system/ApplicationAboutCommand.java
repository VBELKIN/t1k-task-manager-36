package ru.t1k.vbelkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1k.vbelkin.tm.dto.request.ServerAboutRequest;
import ru.t1k.vbelkin.tm.dto.response.ServerAboutResponse;

public class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public final String NAME = "about";

    @NotNull
    public final String ARGUMENT = "-a";

    @NotNull
    public final String DESCRIPTION = "Display developer info.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("[Client]");
        System.out.println("Name: " + getPropertyService().getAuthorName());
        System.out.println("E-mail: " + getPropertyService().getAuthorEmail());
        System.out.println("[Server]");
        @NotNull final ServerAboutRequest request = new ServerAboutRequest(getToken());
        @NotNull final ServerAboutResponse response = getServiceLocator().getSystemEndpoint().getAbout(request);
        System.out.println("Name:  " + response.getName());
        System.out.println("E-mail: " + response.getEmail());
    }

}
