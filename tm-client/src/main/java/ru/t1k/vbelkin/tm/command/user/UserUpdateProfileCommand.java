package ru.t1k.vbelkin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1k.vbelkin.tm.dto.request.UserUpdateProfileRequest;
import ru.t1k.vbelkin.tm.enumerated.Role;
import ru.t1k.vbelkin.tm.model.User;
import ru.t1k.vbelkin.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "update-user-profile";

    @NotNull
    private static final String DESCRIPTION = "update profile of current user";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER UPDATE PROFILE]");
        System.out.print("FIRST NAME: ");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.print("LAST NAME: ");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.print("MIDDLE NAME: ");
        @NotNull final String middleName = TerminalUtil.nextLine();
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(getToken());
        request.setFirstName(firstName);
        request.setLastName(lastName);
        request.setMiddleName(middleName);
        @NotNull final User user = getUserEndpointClient().updateUserProfile(request).getUser();
        showUser(user);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
