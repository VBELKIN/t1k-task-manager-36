package ru.t1k.vbelkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1k.vbelkin.tm.dto.request.ServerVersionRequest;
import ru.t1k.vbelkin.tm.dto.response.ServerVersionResponse;

public class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "version";

    @NotNull
    public static final String ARGUMENT = "-v";

    @NotNull
    public static final String DESCRIPTION = "Display program version.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("[Client]");
        System.out.println(getPropertyService().getApplicationVersion());
        System.out.println("[Server]");
        @NotNull final ServerVersionRequest request = new ServerVersionRequest(getToken());
        @NotNull final ServerVersionResponse response = getServiceLocator().getSystemEndpoint().getVersion(request);
        System.out.println("Client: " + response.getVersion());
    }

}
