package ru.t1k.vbelkin.tm.api.service;

import ru.t1k.vbelkin.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {

}
