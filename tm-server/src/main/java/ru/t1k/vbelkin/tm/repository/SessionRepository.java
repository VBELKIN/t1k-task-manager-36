package ru.t1k.vbelkin.tm.repository;

import ru.t1k.vbelkin.tm.api.repository.ISessionRepository;
import ru.t1k.vbelkin.tm.model.Session;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}
