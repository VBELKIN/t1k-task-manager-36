package ru.t1k.vbelkin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class AbstractResultResponse extends AbstractResponse {

    private Boolean success = true;

    private String message = "";

    public AbstractResultResponse(@NotNull final Throwable throwable) {
        setSuccess(false);
        setMessage(throwable.getMessage());
    }

}
