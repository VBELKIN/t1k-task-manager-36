package ru.t1k.vbelkin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
public class AbstractProjectResponse extends AbstractResponse {

    @Nullable
    private Project project;

    public AbstractProjectResponse(@Nullable final Project project) {
        this.project = project;
    }

}
