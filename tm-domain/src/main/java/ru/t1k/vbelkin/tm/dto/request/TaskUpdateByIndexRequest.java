package ru.t1k.vbelkin.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class TaskUpdateByIndexRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private Integer index;

    public TaskUpdateByIndexRequest(@Nullable String token) {
        super(token);
    }

}
