package ru.t1k.vbelkin.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.model.Project;

@NoArgsConstructor
public class ProjectAddResponse extends AbstractProjectResponse {

    public ProjectAddResponse(@Nullable Project project) {
        super(project);
    }

}
