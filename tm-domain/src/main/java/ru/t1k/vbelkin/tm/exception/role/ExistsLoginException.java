package ru.t1k.vbelkin.tm.exception.role;

public class ExistsLoginException extends AbstractUserException {

    public ExistsLoginException() {
        super("Error! Login already exists...");
    }

}
