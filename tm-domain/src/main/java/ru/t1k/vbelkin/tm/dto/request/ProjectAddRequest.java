package ru.t1k.vbelkin.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
public class ProjectAddRequest extends AbstractUserRequest {

    private Project project;

    public ProjectAddRequest(@Nullable String token) {
        super(token);
    }

}
