package ru.t1k.vbelkin.tm.dto.response;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.model.Task;

import java.util.List;

@Getter
public class TaskListByProjectIdResponse extends AbstractResponse {

    @Nullable
    private final List<Task> tasks;

    public TaskListByProjectIdResponse(@Nullable final List<Task> tasks) {
        this.tasks = tasks;
    }

}
